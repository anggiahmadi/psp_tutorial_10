package com.example;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTests {
	@Autowired
	StudentService service;

	@Test
	public void testSelectAllStudents() {
		List<StudentModel> students = service.selectAllStudents();
		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertEquals("Gagal - size students tidak sesuai", 3, students.size());

	}

	@Test
	public void testSelectStudent() {
		String npm = "10";
		StudentModel student = service.selectStudent(npm);

		Assert.assertNotNull("Gagal = student tidak ditemukan", student);

		Assert.assertEquals("sasa", student.getName());

		Assert.assertEquals("Gagal - GPA student tidak sesuai", 2.0, student.getGpa(), 0.0);

	}

	@Test
	public void testCreateStudent() {
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		// Cek apakah student sudah ada
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));
		// Masukkan ke service
		service.addStudent(student);
		// Cek apakah student berhasil dimasukkan
		Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
	}
	
	@Test
	public void testUpdateStudent(){
		String npm = "10";
		StudentModel student = service.selectStudent(npm);

		Assert.assertNotNull("Gagal = student tidak ditemukan", student);

		Assert.assertEquals("sasa", student.getName());

		Assert.assertEquals("Gagal - GPA student tidak sesuai", 2.0, student.getGpa(), 0.0);
		
		student.setName("New Name");
		student.setGpa(3.0);
		
		service.updateStudent(student);
		
		StudentModel updatedStudent = service.selectStudent(npm);

		Assert.assertEquals("New Name", updatedStudent.getName());

		Assert.assertEquals("Gagal - GPA student tidak sesuai", 3.0, updatedStudent.getGpa(), 0.0);
		
	}
	
	@Test
	public void testDeleteStudent(){
		String npm = "10";
		StudentModel student = service.selectStudent(npm);

		Assert.assertNotNull("Gagal = student tidak ditemukan", student);

		Assert.assertEquals("sasa", student.getName());

		Assert.assertEquals("Gagal - GPA student tidak sesuai", 2.0, student.getGpa(), 0.0);
		
		service.deleteStudent(npm);
		
		StudentModel deletedStudent = service.selectStudent(npm);
		
		Assert.assertNull("Gagal = student still exist", deletedStudent);
	}
}
